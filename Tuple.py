# arr = [1, 2, 3]
# arr[1] = 4
# print(arr)
# tup = (1, 2, 3)
# tup[1] = 4
# print(tup)
# # tup1 = (1,)
# # print(arr.__sizeof__())
# # print(tup.__sizeof__())

# tup = (1, 2, 3)
# arr = list(tup) # [1, 2, 3]
# arr[1] = 4 # [1, 4, 3]
# tup = tuple(arr) # (1, 4, 3)
# print(tup)

# set_ = {1, 2, 3, 4, 5}
# # set()
# arr = [1, 1, 1, 2, 3, 4, 5, 6, 6]
# set_ = set(arr) # {1, 2, 3, 4, 5, 6}
# arr = list(set_)
# print(arr)

# user => {nftId => amount}
# march => ["a", "b", "c", ....]

# dict_ = {"march": ["a", "b", "c"]}

# dict_ = {1: "a", 2: "b"}
arr = [1, 2, 3, 4, 5]

print("a")

# for i in arr:
#     print(arr[i-1])

# for i in range(0, 11):
#     print(i)

# num = int(input("Input your number: "))
# sum = 0
# for i in range(0, num + 1):
#     sum += i
# print(sum)

# arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 1000]
# for i in arr:
#     print(i * i)
# [1, 12, 321, 232324, -123, 0]
# [-123, 0, 1, 12, 321, 232324]

# [3, 2, 1]
# [2, 1, 3] or [3, 1, 2]

arr = [1, 4, 3] # len = 3
#      0  1  2
# arr[len(arr)]
n =len(arr)
for i in range(n - 1):
    for j in range(0, n - 1 - i):


# a = 10
# b = 11
# temp = 0
# temp = a
# a = b
# b = temp

a = 10
b = 11
a, b = b, a

# i = 0, i = 1, i = 2
#  j  j+1 
# [3, 2, 1]
#     j  j+1
# [2, 3, 1]
#        j
# [2, 1, 3]

