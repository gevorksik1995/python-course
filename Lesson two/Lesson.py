a = 100 # int
b = "That is string" # str
c = 10.15457 # float
d = True # bool



print("Integer is: " + str(type(a)))
print("String is: " + str(type(b)))
print("Float is: " + str(type(c)))
print("Boolean is: " + str(type(d)))