
# # List comprehensions
# fruits = ["apple", "banana", "cherry", "kiwi", "mango"]
# newFruits1 = []
# for fruit in fruits:
# 	if "a" in fruit:
# 		newFruits1.append(fruit)
# print(newFruits1)

# newFruits2 = [fruit for fruit in fruits if "a" in fruit]
# print(newFruits2)

# new = []
# for i in range(10):
# 	new.append(i)
# new1 = [i for i in range(10)]
# print(new)
# print(new1)
# # With loop
# # code here


# # List comprehensions
# # code here


# # How list comprehensions works
# # newList = [expression for item in iterable if condition == True]
# # iterables is dataStructures and range() function


# # Lambda and map functions
# # How lambda is works
# # lambda arguments: expression
# # Example
# # code here
# def _sum1(x, y):
# 	return x + y
# _sum = lambda x, y: x + y
# print(_sum(1, 2))
# _list = [i for i in range(15)]
# _list = [i + 1 for i in _list]
# print(_list)



# # Map and Filter functions
# # programm to filter only even items from a list
# # code here
# lst = [i for i in range(10)]
# a = list(map(lambda x: x ** 2, lst))
# print(a)
# def sqr(x):
# 	return x ** 2
# print(list(map(sqr, lst)))

# print(list(filter(lambda x: x % 2 == 0, lst)))
# newLst = []
# for i in lst:
# 	if i % 2 == 0:
# 		newLst.append(i)
# print(newLst)



# # Programm to double each item in a list using map()
# # code here
# def dob(x):
# 	return x * 2
# lst = list(map(lambda x: x * 2, lst))
# print(lst)
# lst = [x * 2 for x in lst]
# print(lst)


# # Map vs ListCompr
# # compr
# # code here


# # map
# # code here

# # Speed difference
# # First example (with lambda function): squares of numbers in range(10000000) - (0...999999)
# # Compr: python -m timeit -r 10 "[x*x for x in range(10000000)]"
# # Map  : python -m timeit -r 10 "list(map(lambda x: x*x, range(10000000)))"
# # Second example (with declarated function): squares of numbers in range(10000000) - (0...999999)
# # Compr: python -m timeit -r 10 -s "def pow2(x): return x*x" "[pow2(x) for x in range(10000000)]"
# # Map  : python -m timeit -r 10 -s "def pow2(x): return x*x" "list(map(pow2, range(10000000)))"
# # compr lambda 538
# # map lambda   832

# # compr func   938
# # map func     873


# [WORD Text hello]
# "WORD Text hello"
# ["WORD", "Text", "hello"]
# ["wordWORD", "textText", "hellohello "]
# ["WORD", "word", "Text", "text", "hello", "hello"]


# SLICE
# a = "hello"
# [START:STOP:STEP]
# a[2:3:]
# a[::2]



def sortStringWithFor(string):
	words = string.split()
	words1 = []
	for word in words:
		words1.append(word.lower() + word)
	print(words1)
	words1.sort()
	words2 = []
	for word in words1:
		words2.append(word[len(word) // 2:])
	
	return " ".join(words2)

def sortStringWithCompr(string):
	words = string.split()
	words = [word.lower() + word for word in words]
	words.sort()
	words = [word[len(word)//2:] for word in words]
	return " ".join(words)

def sortStringWithMap(string):
	words = string.split()
	words = list(map(lambda word: word.lower() + word, words))
	words.sort()
	words = list(map(lambda word: word[len(word)//2:], words))
	return " ".join(words)

# [1, 2, 3, 4, 5, 6, 7, 8, 9]
# arr[0] -> [1, 2, 3]
# arr[2][1] -> 7
# [val for j in range() for i in range]

#
# arr =[
#   [1, 2, 3]
#   [4, 5]
#   [6, 7, 8, 9]
# ]
# [[], [], []]
# 1. arr[0] - sublist -> [1, 2, 3]
# 2. arr[0][0] - val -> arr1.append(val) -> [1]
# 3. arr[0][1] - val -> arr1.append(val) -> [1, 2]
# 4. arr[0][2] - val -> arr1.append(val) -> [1, 2, 3]
# 5. arr[1] - sublist -> [4, 5]
# 6. arr[1][0] - val -> arr1.append(val) -> [1, 2, 3, 4]
# foo, bar

def refactorListFor(matrix):
	arr = []
	for sublist in matrix:
		for val in sublist:
			if val % 2 == 0: 
				arr.append(val ** 2)
	return arr

arr3d = [[[1], [2], [3]], [[4], [5], [6]], [[7], [8], [9]]]
def refactorList(arrs):
	return [val**2 for sublist in arrs for val in sublist if val % 2 == 0]
print([val for subListList in arr3d for sublist in subListList for val in sublist])
# print(sortStringWithFor("WORD Text hello"))
# print(sortStringWithCompr("WORD Text hello"))
# print(sortStringWithMap("WORD Text hello"))
arrs = [[1, 2, 3], [4, 5], [6, 7, 8, 9], [10]]

print(refactorListFor(arrs))
print(refactorList(arrs))