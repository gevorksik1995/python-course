
from point import Point
from math import pi

print(f"pi number is: {pi}")

class Circle():
	def __init__(self, radius, x, y):
		self.radius = radius
		self.point = Point(x, y)
		print(f"Circle radius is: {self.radius}, center is: {self.point.printPoint()}")


	def area(self):
		return pi * (self.radius ** 2)
