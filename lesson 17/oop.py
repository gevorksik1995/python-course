
# class


class Dog():
	""" Simple dog object """

	""" Init when Dog created  """
	def __init__(self, name, age):
		self.name = name
		self.age = age
		print("Dog is been created")
	
	def walk(self):
		print(self.name + " walking")

	def sit(self):
		print(self.name + " sit")

	def jump(self):
		print(self.name + " jump")


""" Create class exemplar """
myDog1 = Dog("Dog", 4) # python calls __init__ method
print(myDog1.age) # print age
print(myDog1.name) # print name
myDog1.walk()
myDog1.sit()
myDog1.jump()
myDog2 = Dog("Dog2", 5)
print(myDog2.age) # print age
print(myDog2.name) # print name
myDog2.walk()
myDog2.sit()
myDog2.jump()