
class Animal():
	def __init__(self, name, age, stamina):
		self.name = name
		self.age = age
		self.stamina = stamina
		print(f"Name is: {self.name}, Age is: {self.age}, Stamina is: {self.stamina}")

	def walk(self):
		self.stamina -= 10
		print("Walk")

	def jump(self):
		self.stamina -= 15
		print("Jump")
	
	
	def pet(self):
		self.stamina += 20
		print("Pet")


	def getStamina(self):
		return self.stamina


class Dog(Animal):
	def __init__(self, name, age, stamina):
		super().__init__(name, age, stamina)

	def talk(self):
		print("Bark")


class Cat(Animal):
	def __init__(self, name, age, stamina):
		super().__init__(name, age, stamina)

	def talk(self):
		print("Meow")


class Cow(Animal):
	def __init__(self, name, age, stamina):
		super().__init__(name, age, stamina)

	def talk(self):
		print("Moo")
	

dog = Dog("Rex", 4, 150)
dog.walk()
dog.jump()
dog.pet()
dog.talk()
cat = Cat("Cat", 2, 50)
cat.walk()
cat.jump()
cat.pet()
cat.talk()
cow = Cow("Cow", 10, 200)
cow.walk()
cow.jump()
cow.pet()
cow.talk()






# dog1 = Dog("Rex", 4, 150)
# dog1.walk()
# print(dog1.getStamina())
# dog1.jump()
# print(dog1.getStamina())
# dog1.pet()
# print(dog1.getStamina())
# dog2 = Dog("Alex", 2, 100)
# dog3 = Dog("Roxy", 5, 123)


"""
		A
		|
	   /|\
	  / | \
      d ct c
"""