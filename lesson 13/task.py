
# O(n)
# O(1)
def sumOfElem_1(num, arr):
	left = 0
	right = len(arr) - 1
	while left < right:
		_sum = arr[left] + arr[right]
		if _sum == num:
			return [arr[left], arr[right]]
		if _sum < num:
			left += 1
		else:
			right += 1
	
	return []


# O(n^2)
# O(1)
def sumOfElem_3(num, arr):
	for i in range(len(arr)):
		for j in range(len(arr)):
			if arr[i] + arr[j] == num:
				return [arr[i], arr[j]]
	return []



arr = [-1, 0, 1, 2, 3, 10, 12, 15, 18]
elem = 28
print(sumOfElem_1(elem, arr))
print(sumOfElem_3(elem, arr))




