# def _sum(num1,num2):
# 	return num1+num2

# def _sum3(num1, num2, num3):
# 	return num1 + num2 + num3

# def sumArgs(*args):
# 	z = 0
# 	for i in args:
# 		z += i
# 	return z

# sumArgs(1, 2)

# def kwargFunc(**kwargs):
# 	print(kwargs)

# kwargFunc(kwargs_1=1, kwargs_2=2, kwargs_3=3)

# def someFunc(num1, num2, *args, **kwargs):
# 	pass

# def someFunc2(*args, num1):
# 	pass

# print(sumArgs(1))
# print(sumArgs(1, 2))
# print(sumArgs(1, 2, 3))
# print(sumArgs(10, -11, 234, 1213, 4432, 124, 28))


# [-1 ,1, 2, 3, 4, 5, 6, 7, 8, 9], k
# k = 3 -> [1, 2]
# k = -100 -> []

def sumOfTwo(elem, arr):
	for i in arr: 
		for j in arr:
			if i + j == elem:
				return [i, j]
	return [] 

print(sumOfTwo(3, [1, 2, 3, 4, 5, 6, 7, 8]))

# [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
#  |
#  k = 4, 1 + x = 4, x = 3 BinarySearch -> 3 ?    

def sumOfTwoN(elem, arr):
	left, right = 0, len(arr) - 1
	while left < right:
		if arr[left] + arr[right] == elem:
			return [arr[left], arr[right]]
		elif arr[left] + arr[right] > elem:
			right -= 1
		else:
			left += 1
	return []

print(sumOfTwoN(4, [1, 2, 4, 5, 6, 8, 9]))
print(sumOfTwoN(5, [1, 2, 4, 5, 6, 8, 9]))



