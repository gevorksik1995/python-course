
# print count of words in strng
# "Hello lower upper hello"
from unicodedata import digit


def wordCount(string):
	return string.count(" ") + 1

print(wordCount("aa bb cc aa bb cc"))

# print count of special symbols in string
# %$#@!?"
# "aaa>>abc##fdsw"
# "#"
# if str[i] == spec[i]:
#	count += 1
# ""
def countSpecSymbols(string):
	spec = "!@#$%^&*()-+?><"
	count = 0
	for i in string:
		for j in spec:
			if i == j:
				count += 1
	return count

print(countSpecSymbols("fhg+-/*%%%++-ghjg+-%"))

# print true if the char at the specefied position in a string is a digit
# ascii 0 == 48, 1 == 49 ... 9 == 57
# "aaa1a"
# index = 10
def firstIsCharDigit(string, index):
	if (index < 0) or (index > len(string)):
		return False
	digits = "0123456789"
	for i in digits:
		if string[index] == i:
			return True
	return False

print(firstIsCharDigit("aa1aa", 2)) # True
print(firstIsCharDigit("aa1aa", 3)) # False

# a
# print(ord("a")) symbol to ascii code
# print(chr(97)) ascii code to symbol
# ascii 0 == 48, 1 == 49 ... 9 == 57
def secondIsCharDigit(string, index):
	if (index < 0) or (index > len(string)):
		return False

	return (ord(string[index]) >= 48) and (ord(string[index]) <= 57)

print(secondIsCharDigit("aa1aa", 2))
print(secondIsCharDigit("aa1aa", 3))


def thirdIsCharDigit(string, index):
	if (index < 0) or (index > len(string)):
		return False
	return string[index].isdigit()

print(thirdIsCharDigit("aa1aa", 2))
print(thirdIsCharDigit("aa1aa", 3))

# "a b c"
# str - "a"
# dict - "a"(key)
forSort = {
	"a": 1,
	"b": 2,
	"c": 3,
	"d": 4,
	"e": 5
}
reverseForSort = {
	1: "a",
	2: "b",
	3: "c",
	4: "d",
	5: "e"
}
# "c d a" -> "a c d"
def sortString(string):
	sortedString = ""
	lst = []
	# "a3cd" -> "1534" -> "1345" 
	for key in forSort.keys():
		for word in string:
			if word == key:
				lst.append(forSort[key])
	
	lst = sorted(lst)
	print(lst) 
	for key in reverseForSort.keys():
		for numWord in lst:
			if numWord == key:
				sortedString = sortedString + " " + reverseForSort[key]

	return sortedString

# [a, d, b] -> [1, 4, 2] -> [1, 2, 4] -> [a, b, d]
print(sortString("e d c"))-