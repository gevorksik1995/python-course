# [12, 13, 14, 15]
# [12, 12.1, "String", True]

a = [12, 2, -1, 4, 45]
print(a)

# [1, 2, 3, 4, 5]
#  0  1  2  3  4
# -5 -4 -3 -2 -1
b = a[2]
print(b)

print(a[-5])

list1 = [12, -2, 123, 21, 243, 24, 14]
list2 = sorted(list1)
print(list2)

print(list1.index(21))
print(list1.index(-1234))
print(max(list1))
print(min(list1))

list1 = [12, 123, 3121, 2124, 1212, -2]
print("list1 is: " + str(list1))
list2 = sorted(list1)
print("list2 is: " + str(list2))
print("list1 is: " + str(list1))

# list3 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
#indexes 0  1  2  3  4  5  6  7  8  9   10  11  12  13  14


# list3[START:STOP:STEP]

print(list3[::])
list1 = [1, 2, 3]
print("list1: " + str(list1))
list2 = list1[:]
list2[2] = 4
print("list2: " + str(list2))
print("list1: " + str(list1))


list3 = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
# print(list3[0][1])
# list3[0][1] = [1, 2, 3][1] = 2
lenght = len(list3)
print(lenght)
string = ["name1", "name2", "name3"]
"name1, name2, name3"
print(len(string))
