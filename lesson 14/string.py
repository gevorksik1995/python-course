


_str = "hello world"
print(_str[4])
print(_str[1:3])
print(_str[::-1])
# [START:STOP:STEP]
#  0123
# "abcd"
#-4-3-2-1
print(_str.capitalize())
_str = "HELLO WORLD"
print(_str.lower())
_str = "helloworld"
print(_str.upper())
print(_str.count("l"))
print(_str.count("hell"))
print(_str.find("l"))
print(_str.rfind("dl"))
print(_str.rfind("ll"))
_str = "Hello {name}"
name = "Alex" 
print(_str.format(name="Alex"))
print(f"Hello {name}")
# a = 57 b = 58
print(ord("a"))
some = chr(57)
print(type(some))
_str = "Hello"
print("".join(f" Hello {name}"))


_str = "text some text some text"
def substr(str, substr):
	count = 0
	if substr in str:
		count += 1
	return count

print(substr(_str, "text"))

def substr2(str, substr):
	textArray = str.split(" ")
	count = 0
	for i in textArray:
		if substr == i:
			count += 1
	return count

print(substr2(_str, "text"))








# _str = "string"

# #   0  1  2  3  4
# #  [1, 2, 3, 4, 5]
# #  -5 -4 -3 -2 -1  
# print(len(_str))
# print(_str[3])
# print(_str[-1])
# # [START:STOP:STEP]
# print(_str[::-1])
# someString = "some string".capitalize()
# print(someString)
# someString = "SOME STRING"
# print(someString.lower())
# someString = "some string".upper()
# print(someString)
# someString = "texttext"

# count = 0
# for i in someString:
# 	if i == "t":
# 		count += 1
# print(count)

# print(someString.count("t"))

# num1 = 1
# num2 = 3
# answer = num1 + num2
# print(str(num1) + " + "+ str(num2) + " = " + str(answer))
# # print(format([1, 2, 3]))
# print(f"{num1} + {num2} = {answer}")


# _str = "apple"
# ascii_list = []
# i = 0
# for i in _str:
# 	print(type(i))
# 	ascii_list.append(ord(i))

# for i in range(len(_str)):
# 	print(type(i))
# 	ascii_list.append(ord(_str[i]))

# print(ascii_list)

# def reverseString(str):
# 	reverseString = ""
# 	index = len(str) - 1
# 	while index >= 0:
# 		reverseString = reverseString + str[index]
# 		index -= 1
# 	return reverseString


# def isPalindrom(str):
# 	if str == reverseString(str):
# 		return True
# 	return False

# def countPalindromInText(text):
# 	count = 0
# 	textList = text.split()
# 	for word in textList:
# 		if isPalindrom(word):
# 			count = count + 1
# 	return count


# print(countPalindromInText("TetxxteT aabaa annb aabaa"))

# print(reverseString("abc"))

# # "aaabba" "bb" -> True
# # "aaabba" "aaa" -> True
# # "aaabba" "aaabbaaaaaaaa" -> False

# def substr(str, sub):
# 	if sub in str:
# 		return True
# 	return False	

# def substrings(str, *args):
# 	pass

# # "aaabbaacc", "aa", "bb", "cc", "aaccvv"
# #  {"aa": True, "bb": True, "cc": True, "aaccvv": False}

# print(substr("aaabbba", "aa"))
